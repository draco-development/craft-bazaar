<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity
 */
class Event
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=false)
     */
    private $datetime;

    /**
     * @var int
     *
     * @ORM\Column(name="pre_event_email_sent", type="integer", nullable=false)
     */
    private $preEventEmailSent = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="post_event_email_sent", type="integer", nullable=false)
     */
    private $postEventEmailSent = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="allow_check", type="integer", nullable=false)
     */
    private $allowCheck = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="application_filepath", type="string", nullable=true, length=255)
     */
    private $applicationFilepath;


    /**
     * One event has many tables. This is the inverse side.
     * @ORM\OneToMany(targetEntity="Table", mappedBy="event")
     */
    private $tables;

    /**
     * @var ReservationLog
     *
     * @ORM\OneToMany(targetEntity="ReservationLog", mappedBy="event")
     */
    private $reservations;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getImage(): string
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage(string $image): void
    {
        $this->image = $image;
    }


    /**
     * @return DateTime
     */
    public function getDatetime(): DateTime
    {
        return $this->datetime;
    }

    /**
     * @param DateTime $datetime
     */
    public function setDatetime(DateTime $datetime): void
    {
        $this->datetime = $datetime;
    }

    /**
     * @return int
     */
    public function getPreEventEmailSent(): int
    {
        return $this->preEventEmailSent;
    }

    /**
     * @param int $preEventEmailSent
     */
    public function setPreEventEmailSent(int $preEventEmailSent): void
    {
        $this->preEventEmailSent = $preEventEmailSent;
    }

    /**
     * @return int
     */
    public function getPostEventEmailSent(): int
    {
        return $this->postEventEmailSent;
    }

    /**
     * @param int $postEventEmailSent
     */
    public function setPostEventEmailSent(int $postEventEmailSent): void
    {
        $this->postEventEmailSent = $postEventEmailSent;
    }

    /**
     * @return mixed
     */
    public function getTables()
    {
        return $this->tables;
    }

    /**
     * @param mixed $tables
     */
    public function setTables($tables): void
    {
        $this->tables = $tables;
    }

    /**
     * @return int
     */
    public function getAllowCheck(): int
    {
        return $this->allowCheck;
    }

    /**
     * @param int $allowCheck
     */
    public function setAllowCheck(int $allowCheck): void
    {
        $this->allowCheck = $allowCheck;
    }

    /**
     * @return string|null
     */
    public function getApplicationFilepath(): ?string
    {
        return $this->applicationFilepath;
    }

    /**
     * @param string $applicationFilepath|null
     */
    public function setApplicationFilepath(?string $applicationFilepath): void
    {
        $this->applicationFilepath = $applicationFilepath;
    }

    /**
     * @return mixed
     */
    public function getReservations()
    {
        return $this->reservations;
    }

    /**
     * @param ReservationLog $reservations
     */
    public function setReservations(ReservationLog $reservations): void
    {
        $this->reservations = $reservations;
    }

}
