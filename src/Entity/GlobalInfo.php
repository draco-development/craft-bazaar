<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GlobalInfo
 *
 * @ORM\Table(name="global_info")
 * @ORM\Entity
 */
class GlobalInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="address1", type="string", length=255, nullable=false)
     */
    private $address1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="address2", type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=2, nullable=false)
     */
    private $state;

    /**
     * @var int
     *
     * @ORM\Column(name="zip", type="integer", nullable=false)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=15, nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="project_name", type="string", length=255, nullable=false)
     */
    private $projectName;

    /**
     * @var string
     *
     * @ORM\Column(name="project_name_short", type="string", length=15, nullable=true)
     */
    private $projectNameShort;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_organization", type="string", length=255, nullable=true)
     */
    private $parentOrganization;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_organization_link", type="string", length=255, nullable=true)
     */
    private $parentOrganizationLink;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAddress1(): string
    {
        return $this->address1;
    }

    /**
     * @param string $address1
     */
    public function setAddress1(string $address1): void
    {
        $this->address1 = $address1;
    }

    /**
     * @return string|null
     */
    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    /**
     * @param string|null $address2
     */
    public function setAddress2(?string $address2): void
    {
        $this->address2 = $address2;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * @return int
     */
    public function getZip(): int
    {
        return $this->zip;
    }

    /**
     * @param int $zip
     */
    public function setZip(int $zip): void
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getProjectName(): string
    {
        return $this->projectName;
    }

    /**
     * @param string $projectName
     */
    public function setProjectName(string $projectName): void
    {
        $this->projectName = $projectName;
    }

    /**
     * @return string|null
     */
    public function getProjectNameShort(): ?string
    {
        return $this->projectNameShort;
    }

    /**
     * @param string $projectNameShort
     */
    public function setProjectNameShort(string $projectNameShort): void
    {
        $this->projectNameShort = $projectNameShort;
    }

    /**
     * @return string|null
     */
    public function getParentOrganization(): ?string
    {
        return $this->parentOrganization;
    }

    /**
     * @param string $parentOrganization
     */
    public function setParentOrganization(string $parentOrganization): void
    {
        $this->parentOrganization = $parentOrganization;
    }

    /**
     * @return string|null
     */
    public function getParentOrganizationLink(): ?string
    {
        return $this->parentOrganizationLink;
    }

    /**
     * @param string $parentOrganizationLink
     */
    public function setParentOrganizationLink(string $parentOrganizationLink): void
    {
        $this->parentOrganizationLink = $parentOrganizationLink;
    }

}
