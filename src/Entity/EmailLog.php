<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * EmailLog
 *
 * @ORM\Table(name="email_log")
 * @ORM\Entity
 */
class EmailLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="`to`", type="string", length=255, nullable=false)
     */
    private $to;

    /**
     * @var string
     *
     * @ORM\Column(name="`subject`", type="string", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="`template`", type="string", length=255, nullable=false)
     */
    private $template;

    /**
     * @var string
     *
     * @ORM\Column(name="`content`", type="text", length=65535, nullable=false)
     */
    private $content;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="`datetime`", type="datetime", nullable=false)
     */
    private $datetime;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @param array $to
     * @return EmailLog
     */
    public function setTo(array $to): EmailLog
    {
        $to = implode(' | ', $to);
        $this->to = $to;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject(): string
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     * @return EmailLog
     */
    public function setSubject(string $subject): EmailLog
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate(): string
    {
        return $this->template;
    }

    /**
     * @param string $template
     * @return EmailLog
     */
    public function setTemplate(string $template): EmailLog
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param array $content
     * @return EmailLog
     */
    public function setContent(array $content): EmailLog
    {
        $content = implode(' | ', $content);
        $this->content = $content;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDatetime(): DateTime
    {
        return $this->datetime;
    }

    /**
     * @param DateTime $datetime
     * @return EmailLog
     */
    public function setDatetime(DateTime $datetime): EmailLog
    {
        $this->datetime = $datetime;
        return $this;
    }
}
