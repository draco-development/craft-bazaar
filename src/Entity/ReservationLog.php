<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JsonSerializable;

/**
 * ReservationLog
 *
 * @ORM\Table(name="reservation_log")
 * @ORM\Entity
 */
class ReservationLog implements JsonSerializable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="checkout_id", type="string", length=255, nullable=false)
     */
    private $checkoutId;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="order_datetime", type="datetime", nullable=false)
     */
    private $orderDatetime;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="business_name", type="string", length=255, nullable=true)
     */
    private $businessName;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=15, nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=2, nullable=false)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=10, nullable=false)
     */
    private $zip;

    /**
     * @var bool
     *
     * @ORM\Column(name="needs_table", type="boolean", length=1, nullable=false)
     */
    private $needsTable = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="direct_seller", type="boolean", length=1, nullable=false)
     */
    private $directSeller = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="needs_electricity", type="boolean", length=1, nullable=false)
     */
    private $needsElectricity = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="table_id", type="integer", nullable=false)
     */
    public $tableId;

    /**
     * @var int
     *
     * @ORM\Column(name="table_qty", type="integer", length=2, nullable=false)
     */
    private $tableQty;

    /**
     * @var int
     *
     * @ORM\Column(name="event_id", type="integer", nullable=false)
     */
    private $eventId;

    /**
     * @var Event
     *
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="reservations")
     */
    private $event;

    /**
     * @var string
     *
     * @ORM\Column(name="items_for_sale", type="string", length=255, nullable=false)
     */
    private $itemsForSale = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="table_assignments", type="string", length=255, nullable=true)
     */
    private $tableAssignments = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean", length=1, nullable=false)
     */
    private $status = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_method", type="string", length=45, nullable=true)
     */
    public $paymentMethod = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_reference", type="string", length=255, nullable=true)
     */
    public $paymentReference = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="string", length=255, nullable=true)
     */
    public $comments = 0;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return ReservationLog
     */
    public function setId(int $id): ReservationLog
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCheckoutId(): string
    {
        return $this->checkoutId;
    }

    /**
     * @param string $checkoutId
     * @return ReservationLog
     */
    public function setCheckoutId(string $checkoutId): ReservationLog
    {
        $this->checkoutId = $checkoutId;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getOrderDatetime(): DateTime
    {
        return $this->orderDatetime;
    }

    /**
     * @param DateTime $orderDatetime
     * @return ReservationLog
     */
    public function setOrderDatetime(DateTime $orderDatetime): ReservationLog
    {
        $this->orderDatetime = $orderDatetime;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ReservationLog
     */
    public function setName(string $name): ReservationLog
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return ReservationLog
     */
    public function setEmail(string $email): ReservationLog
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBusinessName(): ?string
    {
        return $this->businessName;
    }

    /**
     * @param string|null $businessName
     * @return ReservationLog
     */
    public function setBusinessName(?string $businessName): ReservationLog
    {
        $this->businessName = $businessName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }


    /**
     * @param string $phone
     * @return ReservationLog
     */
    public function setPhone(string $phone): ReservationLog
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return int
     */
    public function getTableId(): int
    {
        return $this->tableId;
    }

    /**
     * @param int $tableId
     * @return ReservationLog
     */
    public function setTableId(int $tableId): ReservationLog
    {
        $this->tableId = $tableId;
        return $this;
    }

    /**
     * @return int
     */
    public function getTableQty(): int
    {
        return $this->tableQty;
    }

    /**
     * @param int $tableQty
     * @return ReservationLog
     */
    public function setTableQty(int $tableQty): ReservationLog
    {
        $this->tableQty = $tableQty;
        return $this;
    }


    /**
     * @return int
     */
    public function getEventId(): int
    {
        return $this->eventId;
    }

    /**
     * @param int $eventId
     * @return ReservationLog
     */
    public function setEventId(int $eventId): ReservationLog
    {
        $this->eventId = $eventId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return ReservationLog
     */
    public function setCity(string $city): ReservationLog
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @return ReservationLog
     */
    public function setState(string $state): ReservationLog
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     * @return ReservationLog
     */
    public function setZip(string $zip): ReservationLog
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return bool
     */
    public function getNeedsTable(): bool
    {
        return $this->needsTable;
    }

    /**
     * @param bool $needsTable
     * @return ReservationLog
     */
    public function setNeedsTable(bool $needsTable): ReservationLog
    {
        $this->needsTable = $needsTable;
        return $this;
    }

    /**
     * @return string
     */
    public function getNeedsElectricity(): string
    {
        return $this->needsElectricity;
    }

    /**
     * @param string $needsElectricity
     * @return ReservationLog
     */
    public function setNeedsElectricity(string $needsElectricity): ReservationLog
    {
        $this->needsElectricity = $needsElectricity;
        return $this;
    }

    /**
     * @return string
     */
    public function getItemsForSale(): string
    {
        return $this->itemsForSale;
    }

    /**
     * @param string $itemsForSale
     * @return ReservationLog
     */
    public function setItemsForSale(string $itemsForSale): ReservationLog
    {
        $this->itemsForSale = $itemsForSale;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return ReservationLog
     */
    public function setAddress(string $address): ReservationLog
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return bool
     */
    public function getDirectSeller():bool
    {
        return $this->directSeller;
    }

    /**
     * @param bool $directSeller
     * @return ReservationLog
     */
    public function setDirectSeller(bool $directSeller): ReservationLog
    {
        $this->directSeller = $directSeller;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTableAssignments(): ?string
    {
        return $this->tableAssignments;
    }

    /**
     * @param string $tableAssignments
     * @return ReservationLog|null
     */
    public function setTableAssignments(?string $tableAssignments): ?ReservationLog
    {
        $this->tableAssignments = $tableAssignments;
        return $this;
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return ReservationLog
     */
    public function setStatus(bool $status): ReservationLog
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return Event
     */
    public function getEvent(): Event
    {
        return $this->event;
    }

    /**
     * @param Event $event
     * @return ReservationLog
     */
    public function setEvent(Event $event): ReservationLog
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPaymentMethod(): ?string
    {
        return $this->paymentMethod;
    }

    /**
     * @param string $paymentMethod
     * @return ReservationLog|null
     */
    public function setPaymentMethod(?string $paymentMethod): ?ReservationLog
    {
        $this->paymentMethod = $paymentMethod;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPaymentReference(): ?string
    {
        return $this->paymentReference;
    }

    /**
     * @param string $paymentReference
     * @return ReservationLog|null
     */
    public function setPaymentReference(?string $paymentReference): ?ReservationLog
    {
        $this->paymentReference = $paymentReference;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getComments(): ?string
    {
        return $this->comments;
    }

    /**
     * @param string $comments
     * @return ReservationLog|null
     */
    public function setComments(?string $comments): ?ReservationLog
    {
        $this->comments = $comments;
        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'checkoutId' => $this->checkoutId,
            'address' => $this->address,
            'orderDatetime' => $this->orderDatetime,
            'name' => $this->name,
            'email' => $this->email,
            'businessName' => $this->businessName,
            'phone' => $this->phone,
            'city' => $this->city,
            'state' => $this->state,
            'zip' => $this->zip,
            'needsTable' => $this->needsTable,
            'directSeller' => $this->directSeller,
            'needsElectricity' => $this->needsElectricity,
            'tableId' => $this->tableId,
            'tableQty' => $this->tableQty,
            'eventId' => $this->eventId,
            'itemsForSale' => $this->itemsForSale,
            'tableAssignments' => $this->tableAssignments,
            'status' => $this->status,
            'paymentMethod' => $this->paymentMethod,
            'paymentReference' => $this->paymentReference,
            'comments' => $this->comments,
        ];
    }
}
