<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TemporaryReservationInfo
 *
 * @ORM\Table(name="temporary_reservation_info", indexes={@ORM\Index(name="table_id", columns={"table_id"}), @ORM\Index(name="event_id", columns={"event_id"})})
 * @ORM\Entity
 */
class TemporaryReservationInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="checkout_id", type="string", length=255, nullable=false)
     */
    private $checkoutId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="business_name", type="string", length=255, nullable=true)
     */
    private $businessName;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=15, nullable=false)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=false)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=2, nullable=false)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=10, nullable=false)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="needs_table", type="string", length=1, nullable=false)
     */
    private $needsTable = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="direct_seller", type="string", length=1, nullable=false)
     */
    private $directSeller = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="needs_electricity", type="string", length=1, nullable=false)
     */
    private $needsElectricity = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="items_for_sale", type="text", length=65532, nullable=false)
     */
    private $itemsForSale = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="table_id", type="integer", length=10, nullable=false)
     */
    private $table;

    /**
     * @var int
     *
     * @ORM\Column(name="table_qty", type="integer", length=2, nullable=false)
     */
    private $tableQty;

    /**
     * @var int
     *
     * @ORM\Column(name="event_id", type="integer", length=10, nullable=false)
     */
    private $event;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return TemporaryReservationInfo
     */
    public function setId(int $id): TemporaryReservationInfo
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getCheckoutId(): string
    {
        return $this->checkoutId;
    }

    /**
     * @param string $checkoutId
     * @return TemporaryReservationInfo
     */
    public function setCheckoutId(string $checkoutId): TemporaryReservationInfo
    {
        $this->checkoutId = $checkoutId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return TemporaryReservationInfo
     */
    public function setName(string $name): TemporaryReservationInfo
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return TemporaryReservationInfo
     */
    public function setEmail(string $email): TemporaryReservationInfo
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBusinessName(): ?string
    {
        return $this->businessName;
    }

    /**
     * @param string|null $businessName
     * @return TemporaryReservationInfo
     */
    public function setBusinessName(?string $businessName): TemporaryReservationInfo
    {
        $this->businessName = $businessName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return TemporaryReservationInfo
     */
    public function setPhone(string $phone): TemporaryReservationInfo
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return int
     */
    public function getTable(): int
    {
        return $this->table;
    }

    /**
     * @param int $table
     * @return TemporaryReservationInfo
     */
    public function setTable(int $table): TemporaryReservationInfo
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @return int
     */
    public function getTableQty(): int
    {
        return $this->tableQty;
    }

    /**
     * @param int $tableQty
     * @return TemporaryReservationInfo
     */
    public function setTableQty(int $tableQty): TemporaryReservationInfo
    {
        $this->tableQty = $tableQty;
        return $this;
    }

    /**
     * @return int
     */
    public function getEvent(): int
    {
        return $this->event;
    }

    /**
     * @param int $event
     * @return TemporaryReservationInfo
     */
    public function setEvent(int $event): TemporaryReservationInfo
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return TemporaryReservationInfo
     */
    public function setCity(string $city): TemporaryReservationInfo
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @return TemporaryReservationInfo
     */
    public function setState(string $state): TemporaryReservationInfo
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return string
     */
    public function getZip(): string
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     * @return TemporaryReservationInfo
     */
    public function setZip(string $zip): TemporaryReservationInfo
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @return string
     */
    public function getNeedsTable(): string
    {
        return $this->needsTable;
    }

    /**
     * @param string $needsTable
     * @return TemporaryReservationInfo
     */
    public function setNeedsTable(string $needsTable): TemporaryReservationInfo
    {
        $this->needsTable = $needsTable;
        return $this;
    }


    /**
     * @return string
     */
    public function getNeedsElectricity(): string
    {
        return $this->needsElectricity;
    }

    /**
     * @param string $needsElectricity
     * @return TemporaryReservationInfo
     */
    public function setNeedsElectricity(string $needsElectricity): TemporaryReservationInfo
    {
        $this->needsElectricity = $needsElectricity;
        return $this;
    }

    /**
     * @return string
     */
    public function getItemsForSale(): string
    {
        return $this->itemsForSale;
    }

    /**
     * @param string $itemsForSale
     * @return TemporaryReservationInfo
     */
    public function setItemsForSale(string $itemsForSale): TemporaryReservationInfo
    {
        $this->itemsForSale = $itemsForSale;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return TemporaryReservationInfo
     */
    public function setAddress(string $address): TemporaryReservationInfo
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getDirectSeller(): string
    {
        return $this->directSeller;
    }

    /**
     * @param string $directSeller|null
     * @return TemporaryReservationInfo
     */
    public function setDirectSeller(?string $directSeller): TemporaryReservationInfo
    {
        if(!$directSeller){
            $this->directSeller = "0";
        }
        $this->directSeller = $directSeller;
        return $this;
    }

}
