<?php


namespace App\Service;


use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class CustomQuery
{
    public function getActiveEvents(EntityManagerInterface $entity, int $limit = null){
        $qb = $entity->createQueryBuilder();
        $qb
            ->select('e')
            ->from('App\Entity\Event', 'e')
            ->where('e.datetime >= :datetime')
            ->orderBy('e.datetime', 'ASC')
            ->setParameter('datetime', new DateTime())
        ;

        if($limit){
            $qb->setMaxResults($limit);
        }

        $query = $qb->getQuery();

        return $query->getResult();

    }
}