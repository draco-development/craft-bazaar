<?php


namespace App\Service;


use SquareConnect\Api\CheckoutApi;
use SquareConnect\ApiClient;
use SquareConnect\ApiException;
use SquareConnect\Configuration;
use SquareConnect\Model\CreateCheckoutRequest;
use SquareConnect\Model\CreateOrderRequest;
use SquareConnect\Model\CreateOrderRequestLineItem;
use SquareConnect\Model\Money;

class SquareCheckoutRequest
{
    public function createClient(): CheckoutApi
    {
        // Create and configure a new API client object
        $defaultApiConfig = new Configuration();
        $defaultApiConfig->setAccessToken($_ENV['PERSONAL_ACCESS_TOKEN']);
        $defaultApiConfig->setHost($_ENV['SQUARE_HOST']);
        $defaultApiClient = new ApiClient($defaultApiConfig);
        return new CheckoutApi($defaultApiClient);
    }

    public function createItem(int $cost, string $description, string $quantity): CreateOrderRequestLineItem
    {

        //Create a Money object to represent the price of the line item.
        $price = new Money;
        $price->setAmount($cost * 100);
        $price->setCurrency('USD');

        //Create the line item and set details
        $item = new CreateOrderRequestLineItem;
        $item->setName($description);
        $item->setQuantity($quantity);
        $item->setBasePriceMoney($price);

        return $item;
    }

    public function createOrder(array $items): CreateOrderRequest
    {
        // Create an Order object using line items from above
        $order = new CreateOrderRequest();
        $order->setIdempotencyKey(uniqid()); //uniqid() generates a random string.

        //sets the lineItems array in the order object
        $order->setLineItems($items);

        return $order;
    }

    public function checkout(CreateOrderRequest $order, CheckoutApi $checkoutClient)
    {
        $checkout = new CreateCheckoutRequest();
        $checkout->setIdempotencyKey(uniqid()); //uniqid() generates a random string.
        $checkout->setOrder($order); //this is the order we created in the previous step.
        $checkout->setRedirectUrl($_ENV['REDIRECT_URL']); //Replace with the URL where you want to redirect your customers after transaction.

        try {
            $result = $checkoutClient->createCheckout(
                $_ENV['LOCATION_ID'],
                $checkout
            );

            return $result->getCheckout();

        } catch (ApiException $e) {
            return false;
        }
    }
}
