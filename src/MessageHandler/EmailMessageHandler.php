<?php


namespace App\MessageHandler;


use App\Entity\EmailLog;
use App\Entity\GlobalInfo;
use App\Message\EmailMessage;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use Swift_Attachment;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class EmailMessageHandler extends AbstractController implements MessageHandlerInterface
{
    private $log;
    private $entity;

    public function __construct(LoggerInterface $log, EntityManagerInterface $entity)
    {
        // Setup connection to the logger interface in the event of a fatal error
        $this->log = $log;
        $this->entity = $entity;
    }

    public function __invoke(EmailMessage $email)
    {
        // Cannot connect to swiftmailer the usual way, create a transport manually
        $transport = (new Swift_SmtpTransport($_ENV['MAILER_HOST'], $_ENV['MAILER_PORT'], $_ENV['MAILER_ENCRYPTION']))
            ->setUsername($_ENV['MAILER_USERNAME'])
            ->setPassword($_ENV['MAILER_PASSWORD'])
        ;

        // Connect to swift mailer using the transport
        $mailer = new Swift_Mailer($transport);

        try {
            // Check what type of email we are sending
            if ($email->getIsHtml()) {
                $contentType = 'text/html';
            } else{
                $contentType = 'text/plain';
            }

            // Build the message using the email object's values
            $message = (new Swift_Message($email->getSubject()))
                ->setFrom($email->getFrom(),  $email->getFromName())
                ->setTo($email->getTo(), $email->getToName())
                ->setCc($email->getCc(), $email->getCcToName())
                ->setBcc($email->getBcc(), $email->getBccToName())
                ->setBody(
                    $this->renderView(
                        $email->getTemplate(), $email->getContent()),
                    $contentType
                );

            // Loop through all attachments and add them to the document
            foreach($email->getAttachments() as $attachment) {
                $attachment = Swift_Attachment::fromPath($attachment);
                $message->attach($attachment);
            }

            // Send the email
            $mailer->send($message);

            $emailLog = new EmailLog();
            $emailLog
                ->setDatetime(new DateTime())
                ->setTo($email->getTo())
                ->setContent($email->getContent())
                ->setSubject($email->getSubject())
                ->setTemplate($email->getTemplate())
            ;

            $globalInfo = $this->entity->getRepository(GlobalInfo::class)->findOneBy(['id' => 1]);

            // Don't log the emails to the admin.
            if($email->getTo() !== $globalInfo->getEmail()){
                $this->entity->persist($emailLog);
                $this->entity->flush();
            }

        } catch (Exception $e){

            // Something went wrong. We obviously can not send an email so we will just dump something to the log file for people to find
            $this->log->critical('Could not email an email message! Something is terribly wrong! Output: ' . $e->getMessage());
        }
    }
}
