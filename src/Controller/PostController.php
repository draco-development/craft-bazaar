<?php /** @noinspection PhpComposerExtensionStubsInspection */


namespace App\Controller;


use App\Entity\GlobalInfo;
use App\Entity\Table;
use App\Entity\TemporaryReservationInfo;
use App\Service\SquareCheckoutRequest;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{

    /**
     * @Route("/payment", name="payment", methods={"POST"})
     * @param Request $request
     * @param SquareCheckoutRequest $square
     * @param EntityManagerInterface $entity
     * @return RedirectResponse
     */
    public function payment(Request $request, SquareCheckoutRequest $square, EntityManagerInterface $entity){

        $email = $request->request->get('email');
        $table = $entity->getRepository(Table::class)->findOneBy(['id' => $request->request->get('table')]);
        $quantity = $request->request->get('quantity');
        $lineItems = [];

        //Puts our line item object in an array called lineItems.
        $lineItems[] = $square->createItem($table->getValue(), $table->getDescription(), $quantity);

        // Create an Order object using line items from above
        $order = $square->createOrder($lineItems);

        ///Create Checkout request object.
        $result = $square->checkout($order, $square->createClient());

        if($result){
            // Save this checkout attempt to a temporary table
            /** @noinspection PhpUndefinedMethodInspection */
            $transaction = (new TemporaryReservationInfo())
                ->setAddress($request->request->get('address'))
                ->setCheckoutId($result->getId())
                ->setName($request->request->get('name'))
                ->setEmail($email)
                ->setBusinessName($request->request->get('businessName'))
                ->setPhone($request->request->get('phone'))
                ->setTableQty($quantity)
                ->setTable($table->getId())
                ->setEvent($table->getEvent()->getId())
                ->setCity($request->request->get('city'))
                ->setState($request->request->get('state'))
                ->setZip($request->request->get('zip'))
                ->setNeedsTable($request->request->get('tableNeeded'))
                ->setNeedsElectricity($request->request->get('electricity'))
                ->setItemsForSale($request->request->get('items'))
                ->setDirectSeller($request->request->get('directSeller'))
            ;
            // Save the attempt so we can grab it on their return
            $entity->persist($transaction);
            $entity->flush();

            // Send the user to checkout
            return $this->redirect($result->getCheckoutPageUrl());

        } else {
            return $this->redirect($request->getBaseUrl());
        }
    }

    /**
     * @Route("/contact", name="contactPost", methods={"POST"})
     * @param Request $request
     * @param Swift_Mailer $mailer
     * @param EntityManagerInterface $entity
     * @return RedirectResponse
     */
    public function formPost(Request $request, Swift_Mailer $mailer, EntityManagerInterface $entity){

        $name = $request->request->get('name');
        $phone = $request->request->get('phone');
        $email = $request->request->get('email');
        $message = $request->request->get('message');

        try{
            $globals = $entity->getRepository(GlobalInfo::class)->findAll()[0];
        } catch (Exception $e){
            // If for some reason we cannot connect to the database, send us the email and we can then proceed to diagnose and the message won't get lost
            $globals = new GlobalInfo();
            $globals->setEmail('matt.edholm@dracodevelopmentgroup.com');
        }

        $payload = (new Swift_Message('Message from Craft Bazaar Website'))
            ->setTo($globals->getEmail())
            ->setFrom('info@dracodevelopmentgroup.com','Northrop Craft Bazaar')
            ->setReplyTo($email)
            ->setBody("
                <h2>Question/Comment from Craft Bazaar Website</h2>
                <br>
                <strong>Name: </strong>$name
                <br>
                <strong>Email: </strong>$email
                <br>
                <strong>Phone: </strong>$phone
                <br>
                <strong>Message: </strong><br>
                $message
            ", 'text/html')
            ;

        if($mailer->send($payload)){
            $this->addFlash('success', 'Your message was sent! Somebody will reach out to you as soon as they can!');
            return $this->redirectToRoute('contact');
        } else {
            $this->addFlash('warning', 'There was an error! We could not send your message. Please attempt to email us directly at ' . $globals->getEmail());
            return $this->redirectToRoute('contact');
        }

    }



    public function postToURL($path, $payload, $authorization = null , $jsonDecode = true){

        $ch = curl_init( $path );

        // Setup request to send json via POST.
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, [
            'Content-Type:application/json',
            "$authorization"
        ]);

        // Return response instead of printing.
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

        // Send request.
        $result = curl_exec($ch);
        curl_close($ch);

        if($jsonDecode){
            return json_decode($result);
        }

        return $result;

    }

}
