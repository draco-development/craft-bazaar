<?php
/** @noinspection PhpUnused */

/**
 * Created by IntelliJ IDEA.
 * User: Matt.Edholm
 * Date: 2/7/2019
 * Time: 9:56
 */

namespace App\Controller;


use App\Entity\About;
use App\Entity\AboutCard;
use App\Entity\BannerImage;
use App\Entity\Contact;
use App\Entity\Event;
use App\Entity\GlobalInfo;
use App\Entity\ReservationLog;
use App\Entity\Table;
use App\Entity\TemporaryReservationInfo;
use App\Message\EmailMessage;
use App\Service\CustomQuery;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PublicController
 * @package App\Controller
 * @Route("/", methods={"GET"})
 */
class PublicController extends AbstractController
{
    /**
     * @Route("/", name="home")
     * @param EntityManagerInterface $entity
     * @param CustomQuery $customQuery
     * @return Response
     * @throws Exception
     */
    public function home(EntityManagerInterface $entity, CustomQuery $customQuery){

        $bannerImages = $entity->getRepository(BannerImage::class)->findBy(['isActive' => 1], ['sortOrder' => 'ASC']);
        $events = $customQuery->getActiveEvents($entity, 2);
        $aboutCard = $entity->getRepository(AboutCard::class)->findOneBy(['id' => 1]);

        return $this->render('pages/index.html.twig', [
            'bannerImages' => $bannerImages,
            'events' => $events,
            'card' => $aboutCard,
        ]);
    }

    /**
     * @Route("/event-booking/{id}", name="booking")
     * @param $id
     * @param EntityManagerInterface $entity
     * @return Response
     */
    public function eventBooking($id, EntityManagerInterface $entity){

        $states = json_decode(file_get_contents("https://api.dracodevelopmentgroup.com/v1/us-state-list"));

        $event = $entity->getRepository(Event::class)->findOneBy(['id' => $id]);
        foreach($event->getTables() as $table){
            /** @var Table $table */
            $table->setRemainingTables($table->getId(), $entity);
        }

        return $this->render('pages/booking.html.twig', [
            'event' => $event,
            'eventJson' => $this->json($event)->getContent(),
            'states' => (array)$states->states,
        ]);
    }

    /**
     * @Route("/about", name="about")
     * @param EntityManagerInterface $entity
     * @return Response
     */
    public function about(EntityManagerInterface $entity){

        $pageContent = $entity->getRepository(About::class)->findAll()[0];

        return $this->render('pages/about.html.twig', [
            'pageContent' => $pageContent,
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     * @param EntityManagerInterface $entity
     * @return Response
     */
    public function contact(EntityManagerInterface $entity){

        $pageContent = $entity->getRepository(Contact::class)->findAll()[0];

        return $this->render('pages/contact.html.twig', [
            'pageContent' => $pageContent,
        ]);
    }

    /**
     * @Route("/payments/thank-you", name="thankyou")
     * ?checkoutId={checkoutid}&referenceId={customid}&transactionId={transactionid}
     * @param Request $request
     * @param EntityManagerInterface $entity
     * @param MessageBusInterface $bus
     * @return RedirectResponse|Response
     * @throws Exception
     */
    public function thanks(Request $request, EntityManagerInterface $entity, MessageBusInterface $bus){


        // Extract the query string params that Square sends us
        $checkoutId = $request->get('checkoutId');
        $transactionId = $request->get('transactionId');

        // Require those parameters above to view this page
        if($checkoutId == null || $transactionId == null){
            return $this->redirectToRoute('home');
        }

        // Grab the temporary record so we can save it to the permanent log since
        $temporaryData = $entity->getRepository(TemporaryReservationInfo::class)->findOneBy(['checkoutId' => $checkoutId]);

        // If we couldn't get a temporary record, then kick them out
        if(!$temporaryData){
            return $this->redirectToRoute('home');
        }
        $event = $entity->getRepository(Event::class)->findOneBy(['id' => $temporaryData->getEvent()]);
        // Log the transaction
        $transaction = (new ReservationLog())
            ->setCheckoutId($temporaryData->getCheckoutId())
            ->setOrderDatetime(new DateTime())
            ->setName($temporaryData->getName())
            ->setEmail($temporaryData->getEmail())
            ->setBusinessName($temporaryData->getBusinessName())
            ->setPhone($temporaryData->getPhone())
            ->setTableQty($temporaryData->getTableQty())
            ->setTableId($temporaryData->getTable())
            ->setEventId($temporaryData->getEvent())
            ->setEvent($event)
            ->setCity($temporaryData->getCity())
            ->setState($temporaryData->getState())
            ->setZip($temporaryData->getZip())
            ->setNeedsTable($temporaryData->getNeedsTable())
            ->setNeedsElectricity($temporaryData->getNeedsElectricity())
            ->setItemsForSale($temporaryData->getItemsForSale())
            ->setAddress($temporaryData->getAddress())
            ->setDirectSeller($temporaryData->getDirectSeller())
        ;

        // Save the reservation
        $entity->persist($transaction);

        // Remove the temporarily saved reservation.
        $entity->remove($temporaryData);

        $entity->flush();

        $this->addFlash('notice', 'Your reservation was successful!');

        $event = $entity->getRepository(Event::class)->findOneBy(['id' => $transaction->getEventId()]);
        $globalInfo = $entity->getRepository(GlobalInfo::class)->findOneBy(['id' => 1]);

        $bus->dispatch(new EmailMessage(
            'Northrop (' .$event->getName(). ') Craft Bazaar', // Subject
            [$transaction->getEmail()], // Array of Addresses to send to
            [$transaction->getName()], // Parallel array of names
            [], // Array of CC addresses
            [], // Parallel Array of CC names
            [], // Array of BCC addresses
            [], // Parallel Array of BCC names
            $globalInfo->getEmail(), // From
            $globalInfo->getProjectName(), // From Name
            'emails/reservationConfirmationUser.html.twig', // Email template
            true, // Boolean flag for if the template above is html or not
            [], // Array of files to attach
            null, // Reply to override
            [
                'emailName' => $transaction->getName(),
                'event' => $event->getName(),
                'businessName' => $transaction->getBusinessName(),
                'businessAddress' => $transaction->getAddress(),
                'businessCity' => $transaction->getCheckoutId(),
                'businessZip' => $transaction->getZip(),
                'businessPhone' => $transaction->getPhone(),
                'needsElectric' => $transaction->getNeedsElectricity(),
                'needsTable' => $transaction->getNeedsTable(),
                'tableQuantity' => $transaction->getTableQty()

            ] // Associative Array of variables in the template and their values to send to the template
        ));

        $bus->dispatch(new EmailMessage(
            $transaction->getName() . " Table Reservation Confirmation - " . $event->getName(), // Subject
            [$globalInfo->getEmail()], // Array of Addresses to send to
            [], // Parallel array of names
            [], // Array of CC addresses
            [], // Parallel Array of CC names
            [], // Array of BCC addresses
            [], // Parallel Array of BCC names
            $globalInfo->getEmail(), // From
            $globalInfo->getProjectName(), // From Name
            'emails/reservationConfirmationAdmin.html.twig', // Email template
            true, // Boolean flag for if the template above is html or not
            [], // Array of files to attach
            null, // Reply to override
            [
                'emailName' => $transaction->getName(),
                'event' => $event->getName(),
                'businessName' => $transaction->getBusinessName(),
                'businessAddress' => $transaction->getAddress(),
                'businessCity' => $transaction->getCheckoutId(),
                'businessZip' => $transaction->getZip(),
                'businessPhone' => $transaction->getPhone(),
                'needsElectric' => $transaction->getNeedsElectricity(),
                'needsTable' => $transaction->getNeedsTable(),
                'tableQuantity' => $transaction->getTableQty()
            ] // Associative Array of variables in the template and their values to send to the template
        ));

        $this->addFlash('notice', 'Your reservation was successful!');

        return $this->redirectToRoute('home');
    }

}
