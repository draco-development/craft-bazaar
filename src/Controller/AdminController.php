<?php
/** @noinspection PhpRouteMissingInspection */
/** @noinspection PhpUnused */


namespace App\Controller;

use App\Entity\About;
use App\Entity\AboutCard;
use App\Entity\BannerImage;
use App\Entity\Contact;
use App\Entity\Event;
use App\Entity\GlobalInfo;
use App\Entity\ReservationLog;
use App\Entity\Table;
use App\Entity\User;
use App\Message\EmailMessage;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController
 * @package App\Controller
 * @Route("/admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("/", name="adminHome")
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function adminHome(EntityManagerInterface $em){
        $companyInfo = $em->getRepository(GlobalInfo::class)->findOneBy(['id' => 1]);
        $about = $em->getRepository(About::class)->findOneBy(['id' => 1]);
        $contact = $em->getRepository(Contact::class)->findOneBy(['id' => 1]);
        $bannerImages = $em->getRepository(BannerImage::class)->findAll();
        $users = $em->getRepository(User::class)->findAll();
        $events = $em->createQuery('SELECT e FROM App\Entity\Event e WHERE e.datetime > CURRENT_TIMESTAMP()')->getResult();
        $aboutCard = $em->getRepository(AboutCard::class)->findOneBy(['id' => 1]);

        return $this->render('admin/index.html.twig', [
            'globalInfo' => $companyInfo,
            'aboutPage' => $about,
            'contactPage' => $contact,
            'users' => $users,
            'bannerImages' => $bannerImages,
            'events' => $events,
            'aboutCard' => $aboutCard,
        ]);
    }

    /**
     * @Route("/PostAbout", name="submit_about", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entity
     * @return RedirectResponse
     */
    public function PostAbout(Request $request, EntityManagerInterface $entity){
        $header = $request->request->get('body_header');
        $text = $request->request->get('body_text');

        $about = $entity->getRepository(About::class)->findOneBy(['id' => 1]);

        if(!$about){
            $about = new About();
        }

        $about->setHeader($header);
        $about->setBody($text);
        $entity->persist($about);
        $entity->flush();

        return $this->redirectToRoute('adminHome');
    }

    /**
     * @Route("/PostGlobals", name="submit_globals", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entity
     * @return RedirectResponse
     */
    public function PostGlobals(Request $request, EntityManagerInterface $entity){
        $address1 = $request->request->get('address1');
        $address2 = $request->request->get('address2');
        $city = $request->request->get('city');
        $state = $request->request->get('state');
        $zip = $request->request->get('zip');
        $phone = $request->request->get('phone');
        $email = $request->request->get('email');
        $project = $request->request->get('project');

        $globals = $entity->getRepository(GlobalInfo::class)->findOneBy(['id' => 1]);

        if(!$globals){
            $globals = new GlobalInfo();
        }

        $globals->setAddress1($address1);
        $globals->setAddress2($address2);
        $globals->setCity($city);
        $globals->setState($state);
        $globals->setZip($zip);
        $globals->setPhone($phone);
        $globals->setEmail($email);
        $globals->setProjectName($project);

        $entity->persist($globals);
        $entity->flush();

        return $this->redirectToRoute('adminHome');

    }

    /**
     * @Route("/PostEvents", name="submit_events", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entity
     * @return RedirectResponse
     * @throws Exception
     */
    public function PostEvents(Request $request, EntityManagerInterface $entity){
        $action = $request->request->get('action');
        $name = $request->request->get('eventName');
        $description = $request->request->get('eventDescription');
        $datetime = $request->request->get('eventDate');


        switch($action){
            case 'remove':
                $eventId = $request->request->get('eventId');
                $event = $entity->getRepository(Event::class)->findOneBy(['id' => $eventId]);
                try{
                    unlink($event->getImage());
                } catch (Exception $e){
                    // do nothing
                }
                $entity->remove($event);
                break;
            case 'update':
                $eventId = $request->request->get('eventId');
                $event = $entity->getRepository(Event::class)->findOneBy(['id' => $eventId]);
                $event->setName($name);
                $event->setDescription($description);
                $event->setDatetime(new DateTime($datetime));
                if(($_FILES['eventImageFile']['name'] != "")){
                    $event->setImage("images/event/".$_FILES['eventImageFile']['name']);
                    $this->uploadImage($_FILES['eventImageFile'], '', $event->getImage());
                }
                if($_FILES['applicationFile']['name'] != ""){
                    $this->uploadImage($_FILES['applicationFile'], 'files/', $event->getApplicationFilepath());
                    $event->setApplicationFilepath("files/".$_FILES['applicationFile']['name']);
                }
                $event->setAllowCheck($request->request->get('allowChecks'));
                $entity->persist($event);
                break;
            case 'add':
                $event = new Event();
                $event->setName($name);
                $event->setDescription($description);
                $event->setDatetime(new DateTime($datetime));
                $this->uploadImage($_FILES['eventImageFile'], 'images/event/');
                $event->setImage('images/event/'.$_FILES['eventImageFile']['name']);
                if($checks = $request->request->get('allowChecks')){
                    $this->uploadImage($_FILES['applicationFile'], 'files/');
                    $event->setApplicationFilepath('files/'.$_FILES['applicationFile']['name']);
                    $event->setAllowCheck($checks);
                }

                $entity->persist($event);
                break;
            case 'duplicate':
                $eventId = $request->request->get('eventId');
                $originalEvent = $entity->getRepository(Event::class)->findOneBy(['id' => $eventId]);
                $newEvent = new Event();
                $newEvent->setName($originalEvent->getName() . "copy");
                $newEvent->setDescription($originalEvent->getDescription());
                $newEvent->setDatetime(new DateTime('+12 months'));

                // get image
                $imageName = $originalEvent->getImage();
                // add a -copy
                $imageNameArray = explode('.', $imageName);
                $imageNameArray[sizeof($imageNameArray) - 2] = $imageNameArray[sizeof($imageNameArray) - 2] . '-copy';
                // put it back together
                $imageName = implode(".", $imageNameArray);

                // get image
                $applicationFile = $originalEvent->getImage();
                // add a -copy
                $applicationFileArray = explode('.', $applicationFile);
                $applicationFileArray[sizeof($imageNameArray) - 2] = $applicationFileArray[sizeof($applicationFileArray) - 2] . '-copy';
                // put it back together
                $applicationFile = implode(".", $applicationFileArray);

                // Save duplicate
                copy($originalEvent->getImage(), $imageName);
                copy($originalEvent->getApplicationFilepath(), $applicationFile);
                $newEvent->setImage($imageName);
                $entity->persist($newEvent);
                $entity->flush();
                foreach($originalEvent->getTables() as $originalTable){
                    /**@var Table $originalTable */
                    $newTable = new Table();
                    $newTable->setEvent($newEvent);
                    $newTable->setIsActive(1);
                    $newTable->setQuantity($originalTable->getQuantity());
                    $newTable->setDescription($originalTable->getDescription());
                    $newTable->setValue($originalTable->getValue());
                    $entity->persist($newTable);
                }
                break;
            default:
                $this->addFlash('notice', 'Unsuccessful');
        }

        try{
            $entity->flush();
        } catch (Exception $e){
            if(strstr($e->getMessage(), 'Integrity constraint violation: 1451')){
                $message = 'You cannot delete an event that already has people registered. Please contact support to override.';
            } else {
                $message = "Something went wrong. Please contact support and let us know what you were trying to do.";
            }
            $this->addFlash('notice', $message);
        }

        return $this->redirectToRoute('adminHome');

    }

    /**
     * @Route("/PostTables", name="submit_tables", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entity
     * @return RedirectResponse
     */
    public function PostTables(Request $request, EntityManagerInterface $entity){
        $action = $request->request->get('action');
        $description = $request->request->get('tableDescription');
        $quantity = $request->request->get('tableQuantity');
        $price = $request->request->get('tablePrice');

        switch($action){
            case 'remove':
                $tableId = $request->request->get('tableId');
                $table = $entity->getRepository(Table::class)->findOneBy(['id' => $tableId]);
                $entity->remove($table);
                break;
            case 'update':
                $tableId = $request->request->get('tableId');
                $table = $entity->getRepository(Table::class)->findOneBy(['id' => $tableId]);
                $table->setValue($price);
                $table->setDescription($description);
                $table->setQuantity((int)$quantity);
                $entity->persist($table);
                break;
            case 'add':
                $table = new Table();
                $eventId = $request->request->get('eventId');
                $table->setValue($price);
                $table->setDescription($description);
                $table->setQuantity((int)$quantity);
                $table->setIsActive(1);
                $table->setEvent($entity->getRepository(Event::class)->findOneBy(['id' => $eventId]));
                $entity->persist($table);
                break;
            default:
                $this->addFlash('notice', 'Unsuccessful');
        }

        $entity->flush();

        return $this->redirectToRoute('adminHome');

    }

    /**
     * @Route("/PostContact", name="submit_contact", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entity
     * @return RedirectResponse
     */
    public function PostContact(Request $request, EntityManagerInterface $entity){
        $header = $request->request->get('contact_header');
        $text = $request->request->get('contact_subheader');

        $contact = $entity->getRepository(Contact::class)->findOneBy(['id' => 1]);

        if(!$contact){
            $contact = new About();
        }

        $contact->setHeader($header);
        $contact->setSubheader($text);
        $entity->persist($contact);
        $entity->flush();

        return $this->redirectToRoute('adminHome');

    }

    /**
     * @Route("/PostUser", name="submit_user", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entity
     * @param MessageBusInterface $bus
     * @return RedirectResponse
     * @throws Exception
     */
    public function PostUser(Request $request, EntityManagerInterface $entity, MessageBusInterface $bus){
        $action = $request->request->get('action');
        $username = $request->request->get('username');
        $email = $request->request->get('email');
        $role = $request->request->get('role');
        $globalInfo = $entity->getRepository(GlobalInfo::class)->findOneBy(['id' => 1]);

        // create a password for the new user and reset password actions
        $password = md5(base64_encode((new DateTime('-4 years'))->format('u')));

        switch($action) {
            case 'Remove':
                $userId = $request->request->get('userId');
                $user = $entity->getRepository(User::class)->findOneBy(['id' => $userId]);
                $entity->remove($user);
                break;
            case 'Update':
                $userId = $request->request->get('userId');
                $user = $entity->getRepository(User::class)->findOneBy(['id' => $userId]);
                $user->setUsername($username);
                $user->setEmail($email);
                $user->setRole($role);
                $entity->persist($user);

                $this->addFlash('notice', 'This account has been updated');
                break;
            case 'Add':
                $user = new User();
                $user->setUsername($username);
                $user->setEmail($email);
                $user->setRole($role);
                $user->setPassword($password);
                $entity->persist($user);

                $bus->dispatch(new EmailMessage(
                    "Welcome to " . $globalInfo->getProjectName(),
                    [$email],
                    [],
                    [],
                    [],
                    [],
                    [],
                    $globalInfo->getEmail(),
                    $globalInfo->getEmail(),
                    'emails/newUser.html.twig',
                    true,
                    [],
                    $globalInfo->getEmail(),
                    [
                        'project' => $globalInfo->getProjectName(),
                        'password' => $password,
                    ]
                ));
                $this->addFlash('notice', 'Your password has been emailed to you');

                break;

            case 'Reset Password':

                $bus->dispatch(new EmailMessage(
                    "Password Reset for " . $globalInfo->getProjectName(),
                    [$email],
                    [],
                    [],
                    [],
                    [],
                    [],
                    $globalInfo->getEmail(),
                    $globalInfo->getEmail(),
                    'emails/passwordReset.html.twig',
                    true,
                    [],
                    $globalInfo->getEmail(),
                    [
                        'password' => $password,
                    ]
                ));

                $this->addFlash('notice', 'Your password has been reset. Your new password has been emailed to you');
                break;

            default:
                $this->addFlash('notice', 'Unsuccessful');
        }

        $entity->flush();
        return $this->redirectToRoute('adminHome');

    }

    /**
     * @Route("/PostIndex", name="submit_index", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entity
     * @return RedirectResponse
     */
    public function PostIndex(Request $request, EntityManagerInterface $entity){
        $action = $request->request->get('action');
        $header = $request->request->get('imageHeader');
        $subHeader = $request->request->get('imageSubHeader');
        $sortOrder = $request->request->get('imageSortOrder');

        switch($action){
            case 'remove':
                $imageId = $request->request->get('imageId');
                $image = $entity->getRepository(BannerImage::class)->findOneBy(['id' => $imageId]);
                unlink($image->getImage());
                $entity->remove($image);
                $this->addFlash('notice', 'Success');
                break;
            case 'update':
                $imageId = $request->request->get('imageId');
                $image = $entity->getRepository(BannerImage::class)->findOneBy(['id' => $imageId]);
                $image->setHeader($header);
                $image->setSubheader($subHeader);
                $image->setSortOrder((int)$sortOrder);
                if(isset($_FILES['updateImageFile'])){
                    // overwrite the file
                    $this->uploadImage($_FILES['updateImageFile'], $image->getImage());
                }
                $this->addFlash('notice', 'Success');
                break;
            case 'add':
                $image = new BannerImage();
                $image->setHeader($header);
                $image->setSubheader($subHeader);
                $image->setSortOrder((int)$sortOrder);
                $image->setIsActive(1);
                $this->uploadImage($_FILES['addImageFile'], 'images/banner/');
                $image->setImage('images/banner/'.$_FILES['addImageFile']['name']);
                $entity->persist($image);
                $this->addFlash('notice', 'Success');
                break;
            default:
                $this->addFlash('notice', 'Unsuccessful');
        }

        $entity->flush();

        return $this->redirectToRoute('adminHome');

    }

    /**
     * @Route("/PostReport", name="submit_report", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $entity
     * @param MessageBusInterface $bus
     * @return RedirectResponse
     */
    public function PostReport(Request $request, EntityManagerInterface $entity, MessageBusInterface $bus){

        $eventId = $request->request->get('event');
        $registrants = $entity->getRepository(ReservationLog::class)->findBy(['eventId' => $eventId]);
        $event = $entity->getRepository(Event::class)->findOneBy(['id' => $eventId]);
        $globalInfo = $entity->getRepository(GlobalInfo::class)->findOneBy(['id' => 1]);
        $filename = 'reportsTemporary/'.$event->getName().'.csv';


        // create a file pointer connected to the output stream
        $fp = fopen($filename, 'w', 'w');

        // output the column headings
        fputcsv($fp, [
            'Square Checkout Id',
            'Name',
            'Business Name',
            'Direct Seller',
            'Needs Table',
            'Needs Electricity',
            'Description',
            'Email',
            'Address',
            'City',
            'State',
            'Zip',
            'Phone',
            'Table Qty',
            'Table Description',
            'Order Date'
        ]);

        // loop over the rows, outputting them
        foreach($registrants as $registrant){
            /** @var  ReservationLog $registrant */
            $registrantTable = $entity->getRepository(Table::class)->findOneBy(['id' => $registrant->getTableId()]);
            $array = [
                $registrant->getCheckoutId(),
                $registrant->getName(),
                $registrant->getBusinessName(),
                $registrant->getDirectSeller(),
                $registrant->getNeedsTable(),
                $registrant->getNeedsElectricity(),
                $registrant->getItemsForSale(),
                $registrant->getEmail(),
                $registrant->getAddress(),
                $registrant->getCity(),
                $registrant->getState(),
                $registrant->getZip(),
                $registrant->getPhone(),
                $registrant->getTableQty(),
                $registrantTable->getDescription(),
                $registrant->getOrderDatetime()->format('Y-m-d H:i')
            ];
            fputcsv($fp, $array);
        }
        fclose($fp);

        $bus->dispatch(new EmailMessage(
            'Here is your report from ' . $globalInfo->getProjectName(),
            [$globalInfo->getEmail()],
            [],
            [],
            [],
            [],
            [],
            $globalInfo->getEmail(),
            '',
            'emails/report.html.twig',
            true,
            [$filename],
            null,
            [
                'content' => 'Here is the report you requested'
            ]
        ));

        unlink($filename);
        return $this->redirectToRoute('adminHome');

    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $entity
     * @Route("/PostAboutCard", name="submit_about_card", methods={"POST"})
     * @return RedirectResponse
     */
    public function PostAboutCard(Request $request, EntityManagerInterface $entity){
        $body = $request->request->get('aboutCardBody');
        $header = $request->request->get('aboutCardHeader');
        $aboutCard = $entity->getRepository(AboutCard::class)->findOneBy(['id' => 1]);

        $aboutCard->setHeader($header);
        $aboutCard->setBody($body);

        if($_FILES['aboutCardImage']['name'] != ""){
            // upload file
            move_uploaded_file($_FILES['aboutCardImage']['tmp_name'], 'images/'.$_FILES['aboutCardImage']['name']);
            $aboutCard->setImage('images/'.$_FILES['aboutCardImage']['name']);
        }

        $entity->persist($aboutCard);
        $entity->flush();

        return $this->redirectToRoute('adminHome');

    }

    private function uploadImage(array $image, string $directory, $overwriteImage = null){
        if($overwriteImage){
            move_uploaded_file($image['tmp_name'], $overwriteImage);
        } else {
            move_uploaded_file($image['tmp_name'], $directory.$image['name']);
        }
    }

}


