<?php
/** @noinspection PhpRouteMissingInspection */
/** @noinspection PhpUnused */


namespace App\Controller;

use App\Entity\Event;
use App\Entity\ReservationLog;
use App\Entity\Table;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ManageBookingsController
 * @package App\Controller
 * @Route("/admin")
 */
class ManageBookingsController extends AbstractController
{
    /**
     * @Route("/managebookings", name="manageBookings")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function manageBookingsHome(Request $request, EntityManagerInterface $em)
    {
        $event = $request->get('event');

        $allEvents = $em->createQuery(/** @Lang DQL */'SELECT e FROM App\Entity\Event e WHERE e.datetime > CURRENT_TIMESTAMP()')->getResult();
        if($event){
            $reservations = $em->getRepository(ReservationLog::class)->findBy(['eventId' => $event]);
        } else {
            $reservations = $em->getRepository(ReservationLog::class)->findAll();
        }

        $tables = $em->getRepository(Table::class)->findBy(['isActive' => 1]);

        return $this->render('pages/managebookings.html.twig', [
            'reservations' => $reservations,
            'allEvents' => $allEvents,
            'eventId' => $event,
            'tables' => $tables
        ]);
    }

    /**
     * @Route("/editbooking", name="editBooking", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse
     */
    public function editBooking(Request $request, EntityManagerInterface $em)
    {
        $booking = $em->getRepository(ReservationLog::class)->findOneBy(["id"=>$request->request->get("id")]);
        try{
            if ($booking) {
                $booking->setName($request->request->get('name'));
                $booking->setBusinessName($request->request->get('businessName'));
                $booking->setEmail($request->request->get('email'));
                $booking->setAddress($request->request->get('address'));
                $booking->setPhone($request->request->get('phone'));
                $booking->setCity($request->request->get('city'));
                $booking->setState($request->request->get('state'));
                $booking->setZip($request->request->get('zip'));
                $ds = ((bool)$request->request->get('directSeller'));
                $booking->setDirectSeller($ds);
                $booking->setNeedsTable((bool)$request->request->get('needsTable'));
                $booking->setNeedsElectricity((bool)$request->request->get('needsElectricity'));
                $booking->setItemsForSale($request->request->get('itemsForSale'));
                $booking->setTableAssignments($request->request->get('tableAssignments'));
                $st = (bool)$request->request->get('status');
                $booking->setStatus($st);
                $booking->setTableId($request->request->get("tableId"));
                $booking->setPaymentMethod($request->request->get("paymentMethod"));
                $booking->setPaymentReference($request->request->get("paymentNote"));
                $booking->setComments($request->request->get("comments"));
                $em->persist($booking);

                $em->flush();


            }
        }
        catch (\Exception $ex)
        {
            // If for some reason we cannot connect to the database, send us the email and we can then proceed to diagnose and the message won't get lost
            $debug = true;

        }
        return $this->redirectToRoute("manageBookings");
    }
    /**
     * @Route("/addbooking", name="addBooking", methods={"POST"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return RedirectResponse
     */
    public function addBooking(Request $request, EntityManagerInterface $em)
    {
        try {

            if ($request) {
                $st = (bool)$request->request->get('status');
                $ds = (bool)$request->request->get('directSeller');
                $eventId = (int)($request->request->get('event_selection'));
                $event = $em->getRepository(Event::class)->findOneBy(['id' => $eventId]);
                $transaction = (new ReservationLog())
                    ->setEvent($event)
                    ->setEventId($eventId)
                    ->setCheckoutId("ManualEntry")
                    ->setOrderDatetime(new \DateTime())
                    ->setName($request->request->get('name'))
                    ->setStatus($st)
                    ->setBusinessName($request->request->get('businessName'))
                    ->setEmail($request->request->get('email'))
                    ->setAddress($request->request->get('address'))
                    ->setPhone($request->request->get('phone'))
                    ->setCity($request->request->get('city'))
                    ->setState($request->request->get('state'))
                    ->setZip($request->request->get('zip'))
                    ->setDirectSeller($ds)
                    ->setNeedsTable((bool)$request->request->get('needsTable'))
                    ->setNeedsElectricity((bool)$request->request->get('needsElectricity'))
                    ->setItemsForSale($request->request->get('itemsForSale'))
                    ->setTableAssignments($request->request->get('tableAssignments'))
                    ->setTableQty($request->request->get('tableQuantity'))
                    ->setTableId($request->request->get("tableId"))
                    ->setPaymentMethod($request->request->get('paymentMethod'))
                    ->setPaymentReference($request->request->get('paymentNote'))
                    ->setComments($request->request->get('comments'));

                $em->persist($transaction);
                $em->flush();
            }
        }
        catch (\Exception $ex)
        {
            // If for some reason we cannot connect to the database, send us the email and we can then proceed to diagnose and the message won't get lost
            $debug = true;

        }
        return $this->redirectToRoute("manageBookings");

    }
}
