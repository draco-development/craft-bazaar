<?php


namespace App\Command;


use App\Entity\Event;
use App\Entity\GlobalInfo;
use App\Entity\ReservationLog;
use App\Message\EmailMessage;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class SendPreEventMessage extends Command
{
    protected static $defaultName = 'cron:send-pre-event-messages';
    protected $entity;
    protected $bus;

    protected function configure()
    {
        $this
            ->setDescription("Sends emails to registrants two weeks before an event has started.")
            ->setHelp('This cron will fire and will grab all events that will start within two weeks, and have not yet sent their pre event emails.')
        ;
    }

    public function __construct(EntityManagerInterface $entity, MessageBusInterface $bus, string $name = null)
    {
        parent::__construct($name);
        $this->entity = $entity;
        $this->bus = $bus;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Get events
        $twoWeeksFromNow = (new DateTime('+2 weeks'))->format('Y-m-d H:i:s');
        $events = $this->entity->createQuery("SELECT e FROM App\Entity\Event e WHERE e.datetime > CURRENT_TIMESTAMP() AND e.datetime < :twoWeeks AND e.preEventEmailSent = 0")->setParameter('twoWeeks', $twoWeeksFromNow)->getResult();
        $globalInfo = $this->entity->getRepository(GlobalInfo::class)->findOneBy(['id' => 1]);

        // Get registrants
        foreach($events as $event){
            /* @var $event Event */
            $registrants = $this->entity->getRepository(ReservationLog::class)->findBy(['eventId' => $event->getId()]);
            foreach($registrants as $registrant){
                // Send emails
                $this->bus->dispatch(new EmailMessage(
                    "Northrop Craft Show event instructions", // Subject
                    [$registrant->getEmail()], // Array of Addresses to send to
                    [$registrant->getName()], // Parallel array of names
                    [], // Array of CC addresses
                    [], // Parallel Array of CC names
                    [], // Array of BCC addresses
                    [], // Parallel Array of BCC names
                    $globalInfo->getEmail(), // From
                    $globalInfo->getProjectName(), // From Name
                    'emails/preEvent.html.twig', // Email template
                    true, // Boolean flag for if the template above is html or not
                    [], // Array of files to attach
                    null, // Reply to override
                    [
                        'content' => 'Email body',
                    ] // Associative Array of variables in the template and their values to send to the template
                ));
            }
            $event->setPreEventEmailSent(1);
            $this->entity->persist($event);
        }
        $this->entity->flush();

    }
}
