<?php


namespace App\Extension;

use App\Entity\GlobalInfo;
use App\Entity\Globals;
use App\Entity\Links;
use App\Entity\SocialMedia;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class DatabaseGlobalsExtension extends AbstractExtension implements GlobalsInterface
{
    protected $entity;

    public function __construct(EntityManagerInterface $entity)
    {
        $this->entity = $entity;
    }

    public function getGlobals()
    {

        return [
            'globals' => $this->getGlobalVars(),
        ];
    }

    public function getGlobalVars(){
        return $this->entity->getRepository(GlobalInfo::class)->findAll()[0];
    }
}
