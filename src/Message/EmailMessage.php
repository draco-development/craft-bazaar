<?php


namespace App\Message;


class EmailMessage
{
    private $to;
    private $toName;
    private $isHtml;
    private $from;
    private $fromName;
    private $replyTo;
    private $template;
    private $subject;
    private $content = [];
    private $attachments = [];
    private $cc = [];
    private $ccToName = [];
    private $bcc = [];
    private $bccToName = [];

    /**
     * EmailMessage constructor.
     * @param string $subject
     * @param array $to
     * @param array $toName
     * @param array $cc
     * @param array $ccToName
     * @param array $bcc
     * @param array $bccToName
     * @param string $from
     * @param string $fromName
     * @param string $template
     * @param bool $isHtml
     * @param array $attachments
     * @param string|null $replyTo
     * @param array $content
     */
    public function __construct(string $subject, array $to, array $toName, array $cc, array $ccToName, array $bcc, array $bccToName, string $from, string $fromName, string $template, bool $isHtml, array $attachments, ?string $replyTo, array $content){

        $this->to = $to;
        $this->toName = $toName;
        $this->from = $from;
        $this->fromName = $fromName;
        $this->replyTo = $replyTo;
        $this->template = $template;
        $this->subject = $subject;
        $this->isHtml = $isHtml;
        $this->content = $content;
        $this->attachments = $attachments;
        $this->cc = $cc;
        $this->ccToName = $ccToName;
        $this->bcc = $bcc;
        $this->bccToName = $bccToName;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return mixed
     */
    public function getIsHtml()
    {
        return $this->isHtml;
    }

    /**
     * @param mixed $isHtml
     */
    public function setIsHtml($isHtml): void
    {
        $this->isHtml = $isHtml;
    }

    /**
     * @param mixed $to
     */
    public function setTo($to): void
    {
        $this->to = $to;
    }

    /**
     * @return mixed
     */
    public function getToName()
    {
        return $this->toName;
    }

    /**
     * @param mixed $toName
     */
    public function setToName($toName): void
    {
        $this->toName = $toName;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from): void
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * @param mixed $fromName
     */
    public function setFromName($fromName): void
    {
        $this->fromName = $fromName;
    }

    /**
     * @return mixed
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * @param string|null $replyTo
     */
    public function setReplyTo(?string $replyTo): void
    {
        $this->replyTo = $replyTo;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template): void
    {
        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

    /**
     * @return array
     */
    public function getContent(): array
    {
        return $this->content;
    }

    /**
     * @param array $content
     */
    public function setVariables(array $content): void
    {
        $this->content = $content;
    }

    /**
     * @return array
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * @param array $attachments
     */
    public function setAttachments(array $attachments): void
    {
        $this->attachments = $attachments;
    }

    /**
     * @return array
     */
    public function getCc(): array
    {
        return $this->cc;
    }

    /**
     * @param array $cc
     */
    public function setCc(array $cc): void
    {
        $this->cc = $cc;
    }

    /**
     * @return array
     */
    public function getBcc(): array
    {
        return $this->bcc;
    }

    /**
     * @param array $bcc
     */
    public function setBcc(array $bcc): void
    {
        $this->bcc = $bcc;
    }

    /**
     * @return array
     */
    public function getCcToName(): array
    {
        return $this->ccToName;
    }

    /**
     * @param array $ccToName
     */
    public function setCcToName(array $ccToName): void
    {
        $this->ccToName = $ccToName;
    }

    /**
     * @return array
     */
    public function getBccToName(): array
    {
        return $this->bccToName;
    }

    /**
     * @param array $bccToName
     */
    public function setBccToName(array $bccToName): void
    {
        $this->bccToName = $bccToName;
    }
}
