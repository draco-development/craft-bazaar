module.exports = {
  extends: 'airbnb-base',
  "rules": {
    // windows linebreaks when not in production environment
    "linebreak-style": 'off',
    'no-console': 'off',
  },
  "env": {
    "browser": true,
  },
}