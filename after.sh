#!/bin/sh

# If you would like to do some extra provisioning you may
# add any commands you wish to this file and they will
# be run after the Homestead machine is provisioned.
#
# If you have user-specific configurations you would like
# to apply, you may also create user-customizations.sh,
# which will be run after this script.

echo "-----BEGIN CERTIFICATE-----
MIIFIzCCBAugAwIBAgIQB5hElc9/jxTF01fRjsDjejANBgkqhkiG9w0BAQsFADBN
MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMScwJQYDVQQDEx5E
aWdpQ2VydCBTSEEyIFNlY3VyZSBTZXJ2ZXIgQ0EwHhcNMTYwNzA1MDAwMDAwWhcN
MTkxMDAzMTIwMDAwWjBtMQswCQYDVQQGEwJVUzEQMA4GA1UECBMHSW5kaWFuYTET
MBEGA1UEBxMKRm9ydCBXYXluZTEhMB8GA1UEChMYUnVvZmYgSG9tZSBNb3J0Z2Fn
ZSBJbmMuMRQwEgYDVQQDDAsqLnJ1b2ZmLmNvbTCCASIwDQYJKoZIhvcNAQEBBQAD
ggEPADCCAQoCggEBALVIX8MoekYaiNrJIT2QnjnKHK1BglW34H97uD6mJsohAwzy
/4i60RnIPlvc1bL2v+wC2dnAdR9wNvxbml2mOo/Ihs2yDnEtd8I30rF6xnCRL7cP
62/bAXaTZrP3ATqqli+c0rz0CqHUF8QGSc+cG4UvrnsFkCNSC0L21kD0CU9Jlbtp
AXi2VJU8uRW8igKZx/tDArg4aMRP3HV+iMozB1kxvEJzr7B3627wKwKjrP/escPz
79W8sy6DWFbOktJbvo9ijwNJW9DRV9C/56UgA/po/GDHU8Qnnzc4di0pV1JyP9KX
zAkKqWx6Y6yUKYUEkuyMyfhicv5vndrI0j5IQSsCAwEAAaOCAd0wggHZMB8GA1Ud
IwQYMBaAFA+AYRyCMWHVLyjnjUY4tCzhxtniMB0GA1UdDgQWBBSfSnMysLtdSFBP
hgzYXav54GXXETAhBgNVHREEGjAYggsqLnJ1b2ZmLmNvbYIJcnVvZmYuY29tMA4G
A1UdDwEB/wQEAwIFoDAdBgNVHSUEFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwawYD
VR0fBGQwYjAvoC2gK4YpaHR0cDovL2NybDMuZGlnaWNlcnQuY29tL3NzY2Etc2hh
Mi1nNS5jcmwwL6AtoCuGKWh0dHA6Ly9jcmw0LmRpZ2ljZXJ0LmNvbS9zc2NhLXNo
YTItZzUuY3JsMEwGA1UdIARFMEMwNwYJYIZIAYb9bAEBMCowKAYIKwYBBQUHAgEW
HGh0dHBzOi8vd3d3LmRpZ2ljZXJ0LmNvbS9DUFMwCAYGZ4EMAQICMHwGCCsGAQUF
BwEBBHAwbjAkBggrBgEFBQcwAYYYaHR0cDovL29jc3AuZGlnaWNlcnQuY29tMEYG
CCsGAQUFBzAChjpodHRwOi8vY2FjZXJ0cy5kaWdpY2VydC5jb20vRGlnaUNlcnRT
SEEyU2VjdXJlU2VydmVyQ0EuY3J0MAwGA1UdEwEB/wQCMAAwDQYJKoZIhvcNAQEL
BQADggEBAMKInxSuYYH9ax+fzKNCT1QtzFKARsv6/9/mjYwygUPIboipf6qsWrte
in4ZefwsIFsWKN4yPu3TfJRLAQQbOgnTb+a45duSgBVyFREU7+v6JVTTlEWLqH/d
AA92PU8JHm8lErdcT6fqeVMUlRjghHGcqAl8+vw0BcxTxP3lJpjQTVxdI9vg98pc
qAxMG+EjD4o6wrbvKjLWFKgzJlWuZCFkNQSSbCap4KZlRuR9fMYuvgbscfWP5Ef5
Rsj1+oHaM8/Or/oelo0vkzu2lp82i9CWRsv5cSdVunhGLH9K4vh2CMUMzpZvE7/u
9oDJfmhUVFD36iPGv/Q5839wf/rApEs=
-----END CERTIFICATE-----" | sudo tee /etc/nginx/ssl/star.ruoff.com.crt

echo "-----BEGIN PRIVATE KEY-----
MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQC1SF/DKHpGGoja
ySE9kJ45yhytQYJVt+B/e7g+pibKIQMM8v+IutEZyD5b3NWy9r/sAtnZwHUfcDb8
W5pdpjqPyIbNsg5xLXfCN9KxesZwkS+3D+tv2wF2k2az9wE6qpYvnNK89Aqh1BfE
BknPnBuFL657BZAjUgtC9tZA9AlPSZW7aQF4tlSVPLkVvIoCmcf7QwK4OGjET9x1
fojKMwdZMbxCc6+wd+tu8CsCo6z/3rHD8+/VvLMug1hWzpLSW76PYo8DSVvQ0VfQ
v+elIAP6aPxgx1PEJ583OHYtKVdScj/Sl8wJCqlsemOslCmFBJLsjMn4YnL+b53a
yNI+SEErAgMBAAECggEBAJmw1E9Wn+OTV3d1kZLqatq/uXRB18fobpJmR5wFrVks
h7OQyIgZKnHbTNMA9MQXYjsrZ5qvCh0vO87Tspm29ms0LOVzEb9uNtwYC66gUmDZ
FkTzYD2ixSjMawQkY4xjV5TiNeAwXls4vzVXx3gV91cBR8HQw5TOV/Hb52WF/+aR
JAp19OB3LfNoPUvjqcwiYww/XKccr7C4V3+BtN4HGcXYzg07/TNQJegUMrUAwIC3
0TQ6BAaXsfbr1xgQ8+joZ1QI393tCXHgVBam0wQgABMeYX1SwCuBoLXGzA/u/u7D
/GpW3/yYGwsAi+Jq06GYdHFawpzcBW3hpxa8p1F2vDECgYEA5yh20OkTUBSCTXjq
rHnDJJzJV+EHn9et1af2wKmZQwIXiy91vxPLFh7YHAyO70pKSqy83kxgXQ0pOUPQ
GnT0fR9zsmIXp9t73SvlS3HCJCIm5pkJ5nDo2nfNSh2O+2lxTulWi8srBbyWt0Ws
h+eZuAAQfDS9Q5zOzGsN8DNAUC8CgYEAyMPB7yRwcbJzewdP9Ggrr8BAxSy3pqEN
SZDTew4NF71+93ZWgwWlOa5MOHWzes+z2oai7Zh0voX9PBxiQj8HjFpEj6dWGta9
ytGNJqaO0GB/sXx9Xf/jSnpA7n4c3bjVP2SqHYgQBPt/76hFsnU2Vs9oK1bJTmiS
Pq16/uBGA8UCgYEA4StCagfQPz5rG0pA1peZZm2vl4HlameutqPxsgTC/UnpYzRs
7x85wadz/gahqShlq84OAEDH0XyDfnu3GD569fMQQWBaCLG9O1mJ9u6uHYKDADdt
NGMh/8WZXKvkXrR8olCbdHsIwoLNEWO2R4Br9Zdid4rsHhN+QhO6NRPHfxMCgYBs
KvycSjfDKnCJwsDTYCTZhm3fTl6Mio1MK5ml3UY0mkaEMeAeq7X/w+cRLGlNLAct
IAk8lj2gQTTVZD2Uj70TY7c45uG/iB+t3QV945NitftWt0ockTf28rN1S8FlvKY4
h5YYXyyyYoblqvYSVpC2aP0pDJNdiX3yAyoXlOJkOQKBgQDPTxuSDZYiYPhWyNxU
xSnuD0WW3dIqueiHAfh9syx15W7Wo9VNOwR3w3O+LJMqC8hHAh1mWt60aLNTM/sF
F1IDqA3WjP3V2SEQ0jvFt8H5zdxxIceMdeci7o8J/xYVonURoroyUXs8s0kV0yve
plzvHJeCLgs9wxTibC8jnwllwA==
-----END PRIVATE KEY-----" | sudo tee /etc/nginx/ssl/star.ruoff.com.key

sudo sed -i 's/local-dev.ruoff.com.crt;/star.ruoff.com.crt;/g' /etc/nginx/sites-available/local-dev.ruoff.com
sudo sed -i 's/local-dev.ruoff.com.key;/star.ruoff.com.key;/g' /etc/nginx/sites-available/local-dev.ruoff.com

sudo service nginx restart
sudo apt-get install -y php7.0-fpm

sudo sed -i '$a xdebug.remote_enable = 1' /etc/php/7.0/mods-available/xdebug.ini
sudo sed -i '$a xdebug.remote_connect_back = 1' /etc/php/7.0/mods-available/xdebug.ini
sudo sed -i '$a xdebug.remote_port = 9000' /etc/php/7.0/mods-available/xdebug.ini
sudo sed -i '$a xdebug.max_nesting_level = 512' /etc/php/7.0/mods-available/xdebug.ini
sudo sed -i '$a xdebug.remote_host=127.0.0.1' /etc/php/7.0/mods-available/xdebug.ini
sudo sed -i '$a xdebug.remote_autostart = 1' /etc/php/7.0/mods-available/xdebug.ini
sudo sed -i '$a xdebug.remote_host=127.0.0.1' /etc/php/7.1/fpm/conf.d/20-xdebug.ini
sudo sed -i '$a xdebug.remote_autostart = 1' /etc/php/7.1/fpm/conf.d/20-xdebug.ini
sudo sed -i '$a xdebug.remote_host=127.0.0.1' /etc/php/7.2/fpm/conf.d/20-xdebug.ini
sudo sed -i '$a xdebug.remote_autostart = 1' /etc/php/7.2/fpm/conf.d/20-xdebug.ini
sudo sed -i '$a xdebug.remote_host=127.0.0.1' /etc/php/7.3/fpm/conf.d/20-xdebug.ini
sudo sed -i '$a xdebug.remote_autostart = 1' /etc/php/7.3/fpm/conf.d/20-xdebug.ini

sudo service php7.0-fpm restart
sudo service php7.1-fpm restart
sudo service php7.2-fpm restart
sudo service php7.3-fpm restart

sudo apt-get install -y figlet

# Set the new message
sudo figlet -n "Ruoff Home Mortgage MVC Symfony Development Server v1.0" | sudo tee /etc/motd

# Bring the boom
echo 'Please make sure to sync any databases you need.'
echo 'Please contact matt.edholm@ruoff.com if you have any questions.'
echo 'Booooooooom! We are done. You are a hero. I love you.'
