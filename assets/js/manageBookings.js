import "../scss/manageBookings.scss";

console.log(reservations);

$('[data-toggle="tooltip"]').tooltip();



// Select/Deselect checkboxes
const checkboxes = document.querySelectorAll('table tbody input[type="checkbox"]');
const eventSelect = document.getElementById("eventSelect");

// select/deselect all listener
eventSelect.onchange = (e) => {
 const id = e.target.value;
 if(id != 'null'){
   window.location = "/admin/managebookings?event="+id;
 } else {
   window.location = '/admin/managebookings';
 }

};
// if row is unchecked, uncheck select all listeners
checkboxes.forEach(checkbox => {
  checkbox.onchange = () => {
    if (!checkbox.checked) {
      selectAll.checked = false;
    }
  }
});

// on open of edit modal, populate inputs with table row data
const editModalButton = document.getElementById('editModalButton');
const editModal = document.getElementById('editBookingModal');
const id = document.getElementById('id');
const name = document.getElementById('name');
const status = document.getElementById('status');
const businessName = document.getElementById('businessName');
const phone = document.getElementById('phone');
const address = document.getElementById('address');
const email = document.getElementById('email');
const city = document.getElementById('city');
const state = document.getElementById('state');
const zip = document.getElementById('zip');
const directSeller = document.getElementById('directSeller');
const needsTable = document.getElementById('needsTable');
const needsElectricity = document.getElementById('needsElectricity');
const itemsForSale = document.getElementById('itemsForSale');
const tableAssignments = document.getElementById('tableAssignments');
const boothQuantity = document.getElementById('tableQuantity');
const paymentMethod = document.getElementById('paymentMethod');
const paymentNote = document.getElementById('paymentNote');
const comments = document.getElementById('comments');


$('#editBookingModal').on('shown.bs.modal', function (e) {
  const row = e.relatedTarget.dataset.row;
  // get row data
  const reservation = reservations[row];


  // populate inputs
  id.value = reservation.id;
  name.value = reservation.name;
  status.checked = reservation.status;
  businessName.value = reservation.businessName;
  phone.value = reservation.phone;
  address.value = reservation.address;
  email.value = reservation.email;
  city.value = reservation.city;
  state.value = reservation.state;
  zip.value = reservation.zip;
  directSeller.checked = reservation.directSeller;
  needsTable.checked = reservation.needsTable;
  needsElectricity.checked = reservation.needsElectricity;
  itemsForSale.value = reservation.itemsForSale;
  tableAssignments.value = reservation.tableAssignments;
  boothQuantity.value = reservation.tableQty;
  paymentNote.value = reservation.paymentReference;
  comments.value = reservation.comments;
  paymentMethod.value = reservation.paymentMethod;
});

