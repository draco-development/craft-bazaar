import '../scss/booking.scss';


const tableQuantity = document.getElementById("quantitySelect");
const tableName = document.getElementById("tableSelect");
const sendMessageButton = document.getElementById("sendMessageButton");

tableName.oninput = () => {
  event.tables.forEach(table => {
    let options = '';
    

    if (table.id == tableName.value) {
      if (table.remainingTables < 1) {
        options += "<option value='null' disabled selected>No tables available</option>";
        sendMessageButton.disabled = true;
      }
      else if (table.remainingTables > 3 && tableQuantity.dataset.admin == 1) {
        for (let i = 0; i < table.remainingTables; i++) {
          options += `<option value="${i+1}">${i+1}</option>`;
        }
        sendMessageButton.disabled = false;
      }
      else if (table.remainingTables > 2) {
        for (let i = 0; i < 3; i++) {
          options += `<option value="${i+1}">${i+1}</option>`;
        }
        sendMessageButton.disabled = false;
      } else {
        for (let i = 0; i < table.remainingTables; i++) {
          options += `<option value="${i+1}">${i+1}</option>`;
        }
        sendMessageButton.disabled = false;
      }
      tableQuantity.innerHTML = options;
    }
    tableQuantity.selectedIndex = 0;

  });
}

tableQuantity.onchange = () => {
  console.log(tableQuantity.value);
}