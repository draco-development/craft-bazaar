import "../scss/admin.scss";

var selector = document.getElementById("selector");
var forms = document.getElementsByClassName("form");
selector.addEventListener("input", function() {
  for (let i = 0; i < forms.length; i++) {
    if (forms[i].id === selector.value) {
      // if the selection matches the id, show the form
      forms[i].style.display = "block";
    } else {
      // otherwise hide the form
      forms[i].style.display = "none";
    }
  }
});
